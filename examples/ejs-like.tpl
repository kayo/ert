<ul><%
  for item in items %>
  <li><%
    if item.url ~ /^http/
      %><a href="<%= item.url %>"<%
        if item.description?
          %> title="<%= item.description %>"<%
        end
        %>><%
        if item.title?
          %><%= item.title %><%
        end
        %><%
        if !(item.title?)
          %><%= (item.url ~> /[^\:]*\:\/\/([^\/]+)/)[0] %><%
        end
      %></a><%
    end
    %><%
    if !(item.url ~ /^http/)
      %><button link="<%= item.url %>"><%= item.title %></button><%
    end
    %></li><%
  end %>
</ul>
