{<%
  for res, num in assets
    %>
  "<%=
      res.name
    %>": "<%
      if #root>0
        %><%=
          root
        %>/<%
      end
    %><%=
      prefix[res.type]
    %><%=
      res.name
    %><%
      if suffix[res.type]?
        %><%=
          suffix[res.type]
        %><%
      end
    %>"<%
      if num+1<#assets
        %>,<%
      end
    %><%
  end
%>
}
